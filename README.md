# My Bus
App that trucks your local bus current location 
(map is zoomed out)

![Screenshot 2021-10-27 131254](https://user-images.githubusercontent.com/88953222/139114939-9e0ecbc3-435f-4b57-86cc-4e3a8e2c7541.png)

How it's made Tech used: HTML, CSS, JavaScript, Bootstrap, Node.js, Express, MongoDB. Using these tools to create one of my favorite projects. Tracking the GPS IP to get live location of the Bus while you wait. 

Lesson Learned No matter what your experience level, being an engineer means continuously learning. Learned a lot about GeoLocation.
